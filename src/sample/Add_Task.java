package sample;

import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.time.LocalDate;

public class Add_Task {
    public static void display(ObservableList<Task> g, TableView<Task> g2, Label g3, ChoiceBox choiceBoxRole) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add task");

        Label labelName = new Label("tasks Title");
        Label labelStartDate = new Label("Start Date");
        Label labelEndDate = new Label("End Date");
        Button buttonConfrim = new Button("Confirm");
        TextField fieldName = new TextField();
        Label labeltasksDescription = new Label("tasks Descript");

        TextField fieldtasksDescription = new TextField();

        DatePicker fieldStartDate = new DatePicker();
        DatePicker fieldEndDate = new DatePicker();

        labelName.setPrefSize(100, 30);
        labelStartDate.setPrefSize(100, 30);
        labelEndDate.setPrefSize(50, 30);
        labeltasksDescription.setPrefSize(100, 30);

        fieldtasksDescription.setPrefSize(180, 30);
        fieldName.setPrefSize(180, 30);
        fieldStartDate.setPrefSize(180, 30);
        fieldEndDate.setPrefSize(180, 30);
        buttonConfrim.setPrefSize(240, 30);

        labeltasksDescription.setTranslateX(20);
        labeltasksDescription.setTranslateY(85);
        fieldtasksDescription.setTranslateX(100);
        fieldtasksDescription.setTranslateY(85);
        labelName.setTranslateX(20);
        labelName.setTranslateY(45);
        labelStartDate.setTranslateX(20);
        labelStartDate.setTranslateY(125);
        labelEndDate.setTranslateX(20);
        labelEndDate.setTranslateY(165);

        fieldName.setTranslateX(100);
        fieldName.setTranslateY(45);
        fieldStartDate.setTranslateX(100);
        fieldStartDate.setTranslateY(125);
        fieldEndDate.setTranslateX(100);
        fieldEndDate.setTranslateY(165);
        buttonConfrim.setTranslateX(35);
        buttonConfrim.setTranslateY(215);

        Group root = new Group();
        root.getChildren().add(labelName);
        root.getChildren().add(labelStartDate);
        root.getChildren().add(labelEndDate);
        root.getChildren().add(fieldName);
        root.getChildren().add(fieldStartDate);
        root.getChildren().add(fieldEndDate);
        root.getChildren().add(buttonConfrim);
        root.getChildren().add(labeltasksDescription);
        root.getChildren().add(fieldtasksDescription);

        StackPane final_group = new StackPane(root);
        buttonConfrim.setOnAction(e ->
        {
            if ( fieldName.getText() == "" || fieldtasksDescription.getText() == "" ) {
                new Alert(Alert.AlertType.WARNING, "Name Task , Start Date and End Date fields cannot be empty!").show();
                window.close();
                return;
            }
            if ( fieldStartDate.getValue() == null ) {
                new Alert(Alert.AlertType.CONFIRMATION, "The Date is Wrong!").show();
                window.close();
                return;

            }
            if ( fieldEndDate.getValue() == null ) {
                new Alert(Alert.AlertType.CONFIRMATION, "The Date is Wrong!").show();
                window.close();
                return;
            }

            String name = fieldName.getText();
            String Description = fieldtasksDescription.getText();
            LocalDate StartDate = fieldStartDate.getValue();
            LocalDate EndDate = fieldEndDate.getValue();
            if ( StartDate.getYear() <= EndDate.getYear() ) {
                if ( StartDate.getMonthValue() <= EndDate.getMonthValue() ) {
                    if ( StartDate.getDayOfMonth() <= EndDate.getDayOfMonth() ) {
                        System.out.println("Done");
                    } else {
                        new Alert(Alert.AlertType.CONFIRMATION, "The Date is Wrong! because the date start greater than end date").show();
                        window.close();
                        return;
                    }
                } else {
                    new Alert(Alert.AlertType.CONFIRMATION, "The Date is Wrong! because the date start greater than end date").show();
                    window.close();
                    return;
                }
            } else {
                new Alert(Alert.AlertType.CONFIRMATION, "The Date is Wrong! because the date start greater than end date").show();
                window.close();
                return;
            }
            String state = choiceBoxRole.getSelectionModel().getSelectedItem().toString();
            status vi;
            if ( state.equals("not_Started") ) {
                vi = status.not_Started;
            } else if ( state.equals("InProgress") ) {
                vi = status.InProgress;
            } else {
                vi = status.Done;
            }
            if ( !name.equals("") && !StartDate.equals("") && !EndDate.equals("") ) {

                g.add(new Task(name, StartDate, EndDate, vi, Description));
                g3.setText("Total Task: " + g2.getItems().size());
                fieldName.setText("");
                fieldStartDate.setValue(null);
                fieldEndDate.setValue(null);
                choiceBoxRole.getSelectionModel().selectFirst();
                window.close();
            } else {
                new Alert(Alert.AlertType.WARNING, "Name Task , Start Date and End Date fields cannot be empty!").show();
            }
        });
        Scene scene = new Scene(final_group, 300, 270);
        window.setScene(scene);
        window.showAndWait();


    }
}
