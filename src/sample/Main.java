package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage stage) {


        Button buttonPageTask = new Button("Page Task");
        buttonPageTask.setPrefSize(180, 30);

        Group root = new Group();
        root.getChildren().add(buttonPageTask);

        StackPane final_group = new StackPane(root);
        Scene scene = new Scene(final_group, 500, 500);
        stage.setTitle("Page Project");
        stage.setScene(scene);
        stage.show();
        buttonPageTask.setOnAction(e -> {
            Page_Task.displayPageTasks();
            stage.close();
        });


    }

    public static void main(String[] args) {
        launch(args);
    }

}