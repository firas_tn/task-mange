package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.util.Callback;

import java.time.LocalDate;
import java.util.UUID;

public class Task {
    private String taskID;
    private SimpleStringProperty tasksTitle;
    private LocalDate startDate;
    private LocalDate endDate;
    private status state;
    private SimpleStringProperty tasksDescription;
    private SimpleStringProperty Members;


    public Task() {
        this.taskID = "";
        this.tasksTitle = new SimpleStringProperty("");
        this.tasksDescription = new SimpleStringProperty("");
        this.state = status.not_Started;
    }

    public Task(String name, LocalDate StartDate, LocalDate EndDate, status state, String Description) {
        this.taskID = UUID.randomUUID().toString();
        this.tasksTitle = new SimpleStringProperty(name);
        this.tasksDescription = new SimpleStringProperty(Description);
        this.startDate = StartDate;
        this.Members = new SimpleStringProperty(null);
        this.endDate = EndDate;
        this.state = state;
    }

    public Task(String name, LocalDate StartDate, LocalDate EndDate, status state, String Description, String Members) {
        this.taskID = UUID.randomUUID().toString();
        this.tasksTitle = new SimpleStringProperty(name);
        this.tasksDescription = new SimpleStringProperty(Description);
        this.Members = new SimpleStringProperty(Members);
        this.startDate = StartDate;
        this.endDate = EndDate;
        this.state = state;
    }

    public Task(String name, LocalDate StartDate, LocalDate EndDate, status state, String taskID, String Description, int d) {
        this.taskID = taskID;
        this.tasksTitle = new SimpleStringProperty(name);
        this.tasksDescription = new SimpleStringProperty(Description);
        this.startDate = StartDate;
        this.Members = new SimpleStringProperty(null);
        this.endDate = EndDate;
        this.state = state;
    }

    public Task(String name, LocalDate StartDate, LocalDate EndDate, status state, String taskID, String Description, String Members) {
        this.taskID = taskID;
        this.tasksTitle = new SimpleStringProperty(name);
        this.tasksDescription = new SimpleStringProperty(Description);
        this.Members = new SimpleStringProperty(Members);
        this.startDate = StartDate;
        this.endDate = EndDate;
        this.state = state;
    }

    public String getMembers() {
        return Members.get();
    }

    public void setMembers(String members) {
        this.Members.set(members);
    }

    public String getTasksDescription() {
        return tasksDescription.get();
    }

    public void setTasksDescription(String tasksDescription) {
        this.tasksDescription.set(tasksDescription);
    }

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public status getState() {
        return state;
    }

    public void setState(status state) {
        this.state = state;
    }

    public String getTasksTitle() {
        return tasksTitle.getValue();
    }

    public void setTasksTitle(String tasksTitle) {
        this.tasksTitle = new SimpleStringProperty(tasksTitle);
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate StartDate) {
        this.startDate = StartDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate EndDate) {
        this.endDate = EndDate;
    }

    public static <S, T> Callback<TableColumn<S, T>, TableCell<S, T>> forTableColumn(T... items) {
        return new Callback<TableColumn<S, T>, TableCell<S, T>>() {

            private Callback<TableColumn<S, T>, TableCell<S, T>> callback = ChoiceBoxTableCell.forTableColumn(items);

            @Override
            public TableCell<S, T> call(TableColumn<S, T> param) {
                ChoiceBoxTableCell<S, T> cell = (ChoiceBoxTableCell<S, T>) this.callback.call(param);
                cell.setEditable(true);
                return cell;
            }

        };
    }

    @Override
    public String toString() {
        return "models.Task{" +
                "taskID='" + taskID + '\'' +
                ", tasksTitle='" + tasksTitle + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", status='" + String.valueOf(state) + '\'' +
                '}';
    }

}