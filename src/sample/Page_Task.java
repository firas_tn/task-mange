package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javafx.scene.control.TableColumn;


import java.time.LocalDate;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;

public class Page_Task {
    public static void displayPageTasks() {
        Stage stage = new Stage();
        status[] Statues = {status.not_Started, status.InProgress, status.Done};

        Label labelTotalUsers = new Label("Total Task: 0");
        Button buttonUpDate = new Button("update Task");
        Button buttonToPrint = new Button("Print All Task On Console");

        Button buttonAdd = new Button("Create Task");
        Button buttonDelete = new Button("Delete Selected Task");
        Button buttonDeleteAll = new Button("Delete All Task");

        TableView<Task> table = new TableView();

        ChoiceBox choiceBoxRole = new ChoiceBox(FXCollections.observableArrayList(Statues));


        ObservableList<Task> data = FXCollections.observableArrayList();

//************************************************************************************
        TableColumn<Task, String> columnName = new TableColumn<>("tasks Title");
        columnName.setCellValueFactory(new PropertyValueFactory<>("tasksTitle"));
//************************************************************************************

//************************************************************************************

        TableColumn<Task, LocalDate> columnStart_Date = new TableColumn<>("Start Date");
        columnStart_Date.setCellValueFactory(new PropertyValueFactory<>("startDate"));
//************************************************************************************

//************************************************************************************

        TableColumn<Task, LocalDate> columnEnd_Date = new TableColumn<>("End Date");
        columnEnd_Date.setCellValueFactory(new PropertyValueFactory<>("endDate"));

//************************************************************************************

//************************************************************************************
        TableColumn<Task, status> columnState = new TableColumn<>("status");
        columnState.setCellValueFactory(new PropertyValueFactory<>("state"));
        columnState.setCellFactory(Task.forTableColumn(Statues));
        columnState.setOnEditCommit((CellEditEvent<Task, status> t) -> {
            ((Task) t.getTableView()
                    .getItems()
                    .get(t.getTablePosition().getRow()))
                    .setState(t.getNewValue());
        });
//************************************************************************************

//************************************************************************************
        TableColumn<Task, String> columnID = new TableColumn<>("ID");
        columnID.setCellValueFactory(new PropertyValueFactory<>("taskID"));

//************************************************************************************

//************************************************************************************
        TableColumn<Task, String> columntasksDescription = new TableColumn<>("tasks Description");
        columntasksDescription.setCellValueFactory(new PropertyValueFactory<>("tasksDescription"));
//************************************************************************************

//************************************************************************************
        TableColumn<Task, String> columnMembers = new TableColumn<>("Members");
        columnMembers.setCellValueFactory(new PropertyValueFactory<>("Members"));

        table.setEditable(true);
        table.getColumns().addAll(columnID, columnName, columntasksDescription, columnStart_Date, columnEnd_Date, columnState, columnMembers);
        table.setItems(data);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        table.setPrefSize(1205, 275);
        columnID.setPrefWidth(245);
        columnName.setPrefWidth(100);
        columnStart_Date.setPrefWidth(180);
        columnEnd_Date.setPrefWidth(180);
        columnState.setPrefWidth(100);
        columntasksDescription.setPrefWidth(200);
        columnMembers.setPrefWidth(200);

        buttonUpDate.setPrefSize(180, 30);
        buttonAdd.setPrefSize(180, 30);
        buttonToPrint.setPrefSize(180, 30);
        labelTotalUsers.setPrefSize(180, 30);
        buttonDelete.setPrefSize(180, 30);
        buttonDeleteAll.setPrefSize(180, 30);

        table.setTranslateX(245);
        table.setTranslateY(25);

        labelTotalUsers.setTranslateX(30);
        labelTotalUsers.setTranslateY(280);

        buttonAdd.setTranslateX(30);
        buttonAdd.setTranslateY(75);

        buttonDelete.setTranslateX(30);
        buttonDelete.setTranslateY(115);

        buttonDeleteAll.setTranslateX(30);
        buttonDeleteAll.setTranslateY(155);

        buttonUpDate.setTranslateX(30);
        buttonUpDate.setTranslateY(195);

        buttonToPrint.setTranslateX(30);
        buttonToPrint.setTranslateY(240);

        Group root = new Group();
        choiceBoxRole.getSelectionModel().selectFirst();
        ImageView imageView;
        Image image = new Image(Page_Task.class.getResourceAsStream("/resources/PlanettoLogo.png"));
        imageView = new ImageView(image);

        imageView.setFitHeight(200);
        imageView.setFitWidth(200);

        imageView.setTranslateX(20);
        imageView.setTranslateY(-60);

        root.getChildren().add(table);
        root.getChildren().add(buttonAdd);
        root.getChildren().add(labelTotalUsers);
        root.getChildren().add(buttonDelete);
        root.getChildren().add(buttonDeleteAll);
        root.getChildren().add(buttonUpDate);
        root.getChildren().add(imageView);
        root.getChildren().add(buttonToPrint);

        StackPane final_group = new StackPane(root);
        Scene scene = new Scene(final_group, 1500, 400);
        stage.setTitle("Page Tasks");
        stage.setScene(scene);
        stage.show();


        buttonDelete.setStyle("        -fx-background-color: \n" +
                "        #090a0c,\n" +
                "        linear-gradient(#38424b 0%, #1f2429 20%, #191d22 100%),\n" +
                "        linear-gradient(#20262b, #191d22),\n" +
                "        radial-gradient(center 50% 0%, radius 100%, rgba(114,131,148,0.9), rgba(255,255,255,0));\n" +
                "    -fx-background-radius: 5,4,3,5;\n" +
                "    -fx-background-insets: 0,1,2,0;\n" +
                "    -fx-text-fill: white;\n" +
                "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );\n" +
                "    -fx-font-family: \"Arial\";\n" +
                "    -fx-text-fill: linear-gradient(white, #d0d0d0);\n" +
                "    -fx-font-size: 12px;\n");

        buttonDeleteAll.setStyle("        -fx-background-color: \n" +
                "        #090a0c,\n" +
                "        linear-gradient(#38424b 0%, #1f2429 20%, #191d22 100%),\n" +
                "        linear-gradient(#20262b, #191d22),\n" +
                "        radial-gradient(center 50% 0%, radius 100%, rgba(114,131,148,0.9), rgba(255,255,255,0));\n" +
                "    -fx-background-radius: 5,4,3,5;\n" +
                "    -fx-background-insets: 0,1,2,0;\n" +
                "    -fx-text-fill: white;\n" +
                "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );\n" +
                "    -fx-font-family: \"Arial\";\n" +
                "    -fx-text-fill: linear-gradient(white, #d0d0d0);\n" +
                "    -fx-font-size: 12px;\n");
        buttonAdd.setStyle("      -fx-background-color: \n" +
                "        #000000,\n" +
                "        linear-gradient(#7ebcea, #2f4b8f),\n" +
                "        linear-gradient(#426ab7, #263e75),\n" +
                "        linear-gradient(#395cab, #223768);\n" +
                "    -fx-background-insets: 0,1,2,3;\n" +
                "    -fx-background-radius: 3,2,2,2;\n" +
                //   "    -fx-padding: 12 30 12 30;\n" +
                "    -fx-text-fill: white;\n" +
                "    -fx-font-family: Galada;" +
                "    -fx-font-size: 12px;");

        buttonUpDate.setStyle("        -fx-background-color: \n" +
                "        #090a0c,\n" +
                "        linear-gradient(#38424b 0%, #1f2429 20%, #191d22 100%),\n" +
                "        linear-gradient(#20262b, #191d22),\n" +
                "        radial-gradient(center 50% 0%, radius 100%, rgba(114,131,148,0.9), rgba(255,255,255,0));\n" +
                "    -fx-background-radius: 5,4,3,5;\n" +
                "    -fx-background-insets: 0,1,2,0;\n" +
                "    -fx-text-fill: white;\n" +
                "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );\n" +
                "    -fx-font-family: \"Arial\";\n" +
                "    -fx-text-fill: linear-gradient(white, #d0d0d0);\n" +
                "    -fx-font-size: 12px;\n"
        );
        buttonToPrint.setStyle(
                "    -fx-font-size: 12px;"
        );



        //columnEmail.setStyle("-fx-background-color: #f000ff");

        buttonAdd.setOnAction(e -> Add_Task.display(data, table, labelTotalUsers, choiceBoxRole));
        buttonToPrint.setOnAction(e -> {
            int j = 1;
            for (Task i : data) {
                System.out.println("********************************************************************{{{{" + "Task Number :" + j + "}}}}********************************************************************");
                System.out.println(i.toString());
                System.out.println("***************************************************************************************************************************************************************************");
                System.out.println("");

                j++;
            }
            if ( data.isEmpty() ) {
                System.out.println("Dont Have Any Task");
            }
            // (table.getSelectionModel().getSelectedItem()).getTaskID();
        });
        columnName.setStyle("-fx-alignment:CENTER");
        columnEnd_Date.setStyle("-fx-alignment:CENTER");
        columnID.setStyle("-fx-alignment:CENTER");
        columnMembers.setStyle("-fx-alignment:CENTER");
        columnState.setStyle("-fx-alignment:CENTER");
        columntasksDescription.setStyle("-fx-alignment:CENTER");
        columnStart_Date.setStyle("-fx-alignment:CENTER");

        buttonDelete.setOnAction(e -> {
            table.getItems().removeAll(table.getSelectionModel().getSelectedItems());
            labelTotalUsers.setText("Total Task: " + table.getItems().size());
        });

        buttonDeleteAll.setOnAction(e -> {
            table.getItems().clear();
            labelTotalUsers.setText("Total Task: " + table.getItems().size());
        });

        buttonUpDate.setOnAction(e ->
        {
            if ( data.isEmpty() ) {
                new Alert(Alert.AlertType.WARNING, "No Have Any Task In Page Task").show();
            } else {
                Up_Date.display(data, table, labelTotalUsers, choiceBoxRole);
            }
        });

    }

    }
